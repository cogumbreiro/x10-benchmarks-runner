BENCHMARK_URL = https://bitbucket.org/cogumbreiro/x10-benchmarks

all: compile

compile: benchmarksuite armus-x10/target/armusc-x10.jar
	(cd benchmarksuite; make checked)

run: logs/origin logs/checked benchmarktk compile
	./benchmarktk/run-benchmark setup.yaml run.yaml

collect: stats-tk
	./benchmarktk/collect-data setup.yaml stats.yaml

recompile:
	(cd armus-x10; ant)
	(cd benchmarksuite; make clean; make checked)

data:
	mkdir -p data

logs/checked:
	mkdir -p logs/checked

logs/origin:
	mkdir -p logs/origin

armus-x10/target/armusc-x10.jar: armus-x10
	(cd armus-x10; ant)

sync: armus-x10 benchmarksuite benchmarktk stats-tk
	(cd armus-x10; git pull)
	(cd benchmarksuite; git pull)
	(cd benchmarktk; git pull)
	(cd stats-tk; git pull)

armus-x10:
	git clone https://bitbucket.org/cogumbreiro/armus-x10/

benchmarksuite:
	git clone $(BENCHMARK_URL) $@

benchmarktk:
	git clone https://bitbucket.org/cogumbreiro/benchmarktk/ 

stats-tk:
	git clone https://bitbucket.org/cogumbreiro/stats-tk/
